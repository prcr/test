# Technical Writing Test

<https://prcr.gitlab.io/test>

## Building the site locally

Follow these instructions to build the site locally on your machine:

1. Make sure that you have a [supported version of Node.js](https://nodejs.org/en/about/releases/) or [install Node.js](https://nodejs.org/en/learn/getting-started/how-to-install-nodejs) if the following command fails with an error:

    ```bash
    $ node -v
    v20.11.0
    ```

2. Clone this repository and change into the local copy directory:

    ```bash
    git clone https://gitlab.com/prcr/test.git
    cd test
    ```

3. Install all necessary dependencies:

    ```bash
    npm install
    ```

4. Build the site using Antora:

    ```bash
    npx antora antora-playbook.yml
    ```

The static HTML files are now available on `build/site/`.
